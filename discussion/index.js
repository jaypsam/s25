//

/*
	JSON Object
		- JSON statnds for Javascript Object Notation
		- JSON is also used for other programming languages hence the name Javascript Object Notation

	Syntax:
		{
			"propertyA": "valueA"
			"propertyB": "valueB"
		}	
*/

//JSON Object
/*
{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}
*/

//JSON Array
/*
"cities" = [
	{"city":"Quezon City", "province": "Metro Manila", "country": "Philippines"},
	{"city":"Manila", "province": "Metro Manila", "country": "Philippines"},
	{"city":"Makati City", "province": "Metro Manila", "country": "Philippines"}
]
*/
//JSON Methods
/*
	The JSON Object contains methods for parsing and converting data into stringified JSON
*/

//Converting data into stringified JSON

let batchArray = [
	{batchName: "Batch 197"},
	{batchName: "Batch 198"}
]

console.log(batchArray);
console.log("Result from stringify method:")
console.log(JSON.stringify(batchArray))

let userProfile = JSON.stringify({
	name: "John",
	age: 31,
	address:{
		city: "Manila",
		regoin: "NCR",
		country: "Philippines"

	}
})

console.log("Result from stringify method (object)")
console.log(userProfile)

//User Details
// let firstName = prompt("What is your first name?");
// let lastName = prompt("What is your last name?");
// let age = prompt("What is your age?");
// let address = {
// 	city: prompt("Which do you live in?"),
// 	country: prompt("Which country does your city address belong to?")
// }

// console.log(" ");
// let userData = JSON.stringify({
// 	firstName: firstName,
// 	lastName: lastName,
// 	age: age,
// 	address: address 
// })

//console.log(userData);

// Convert stringified JSON into JS Objects
// JSON.parse()

let batchesJSON = `[
	{
		"batchName": "Batch 197"
	},
	{
		"batchName": "Batch 198"
	}
]`
console.log(batchesJSON);
console.log("Results from parse method:");
console.log(JSON.parse(batchesJSON))

let stringifiedObject = `{
	"name": "Ivy",
	"age": "18",
	"address":{
		"city": "Calaocan City",
		"country": "Philippines"
	},
		"address":{
		"city": "Calaocan City",
		"country": "Philippines"
	}
}`

console.log(stringifiedObject);
console.log("Results from parse method (object)")
console.log(JSON.parse(stringifiedObject));






